const assert = require('assert')
const chai = require('chai')
// use the expect assertion from the chai module
const expect = chai.expect

// require the chai-http module to test HTTP request
const http = require('chai-http')
chai.use(http)

// start our app
const app = require('../app')

// make sure our test framework Mocha is working as expected
	// describe() is used to group unit test that are related to each other under one test suite
	// unit test 
	// test suite
xdescribe('Array',  ()=>{
	describe('indexOf()', ()=> {
		// it is a single unit test
		it('should return -1 when the given value is not in the array', ()=> {
			// in a unit test, assertions will be written to tell mocha what should happen in order for the test to pass
			assert.equal([1, 2, 3].indexOf(4), -1)
		})
	})
})

// Pag may x wala na

// test the existence of our application
describe('App', () => {
	it('Should exist', () => {
		expect(app).to.be.a('function')
	})

	// use async to indicate that a function will return a promise object
	it('GET / should return status code 200 and a message', async() => {
		// use the request method of chai-http to send a get request to the '/' endpoint in the app module
		return await chai.request(app).get('/')
		.then((res) => {
			// write assertions for the response returned by our async function
			expect(res).to.have.status(200)
			expect(res.body.message).to.contain('true')
		})
	})

	// describe a test suite with unit test covering user registration
	describe('user registration,', () => {	
		it('should return status code 201 and a confirmation for valid input', async() => {
			// mock a valid user input
			const new_user = {
				"name" : "John Wick",
				"email" : "baba@yaga.com",
				"password" : "wheresmydog"
			}

			// use http to send a post request to register endpoint in our app module
			return await chai.request(app).post('/register')
			.type('form')
			// the send() method attaches the mock input to the request
			.send(new_user)
			.then((res) => {
				expect(res).to.have.status(201);
				expect(res.body.message).to.be.equal("User registered.");
				expect(res.body.errors.length).to.be.equal(0);
			})
		})
	})

	describe('user authentication', () => {
		it('should return status code 200 and a JWT', async () => {
			// define a mock user input
			const valid_input = {
				'email' : 'baba@yaga.com',
				'password' : 'wheresmydog'
			}

			// send a post request to the login end point in the app module via chai-http
			return await  chai.request(app).post('/login')
			.type('form')
			.send(valid_input)
			.then((res) => {
				expect(res).to.have.status(200)
				expect(res.body.token).to.exist
				expect(res.body.message).to.be.equal('Successfully authenticated')
				expect(res.body.errors.length).to.be.equal(0);
			})
		})
	})
})