const express = require('express')

// initialize the express app
const app = express()

// require the jsonwebtoken package fot signing JWT's
const jwt = require('jsonwebtoken')

// require body parser to parse input
const bodyParser = require('body-parser')

// middleware to parse requests of type extended urlencoded
app.use(bodyParser.urlencoded({extended:false}));

// middleware to parse requests of type json
app.use(bodyParser.json())

// define routes  in our app
app.get('/', (req, res) => {
	return res.status(200).json({
		message: "true"
	})
})

app.post('/register', (req, res) => {
	// define error flag
	let hasErrors = false;
	let errors = []

	if(!req.body.name) {
		errors.push({
			'name' : "Name was not received"
		})
		hasErrors = true
	}

	if(!req.body.email) {
		errors.push({
			'email' : "Email was not received"
		})
		hasErrors = true
	}

	if(!req.body.password) {
		errors.push({
			'password' : "Password was not received"
		})
		hasErrors = true
	}

	if(hasErrors) {
		return res.status(422).json({
			message: "Invalid input",
			errors: errors
		})
		// unprocessable entity di maka proceed kasi di alam gagawin sa natanggap niya
	} else {
		return res.status(201).json({
			message: "User registered.",
			errors: errors
		})
	}
})

app.post('/login', (req, res) => {
	// define error flag
	let hasErrors = false;
	let errors = []

	if(!req.body.email) {
		errors.push({
			'email' : "Email was not received"
		})
		hasErrors = true
	}

	if(!req.body.password) {
		errors.push({
			'password' : "Password was not received"
		})
		hasErrors = true
	}

	if(hasErrors) {
		return res.status(422).json({
			message: "Invalid input",
			errors: errors
		})
		// unprocessable entity di maka proceed kasi di alam gagawin sa natanggap niya
	} else {
		// check if credentials provided are correct
		if(req.body.email === 'baba@yaga.com' && req.body.password === 'wheresmydog') {
			// we can now sign a JWT and sent it back to the client via our response
			let token = jwt.sign({name: "John Wick"}, 'secretSabiE')
			return res.status(200).json({
				message: 'Successfully authenticated',
				token: token,
				errors: errors
			})
		} else {
			return res.status(401).json({
				message: "Wrong credentials"
			})
		}
	}
})

// export this module
module.exports = app